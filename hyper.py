import json
import time
import os
import sqlite3
import shutil
import libvirt
from random import randint, random

FILE_DB = "hyper.db"
DIR_IMAGES = "images"
DIR_TEMPLATES = "templates"
DIR_MACHINES = "machines"


def _initDB():
    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()

    # Table creation
    try:
        # Machines
        cur.execute(
            "create table machines(id varchar(32), type varchar(32), mem int, image varchar(32), state varchar(32))")
        print("Machines created")
    except:
        print("Machines exists")

    try:
        # Networks
        cur.execute("create table networks(id varchar(32), type varchar(32), addr varchar(32), range varchar(32), bridge varchar(32))")
        print("Network created")
    except:
        print("Network exists")

    try:
        # Connections
        cur.execute("create table connections(mac varchar(32), machine varchar(32), network varchar(32))")
        print("Connections created")
    except:
        print("Connections exists")

    conBD.commit()
    conBD.close()


########################################################
####################### Machines #######################
########################################################

def addMachine(mach):
    print("addMach")
    _initDB()

    if "type" not in mach: raise Exception("Missing type")
    if "image" not in mach: raise Exception("Missing image")
    if not os.path.exists(os.path.join(DIR_IMAGES, mach["image"])): raise Exception("Unknown image")

    mach["mem"] = mach["mem"] if "mem" in mach else 1048576
    mach["id"] = str(time.time_ns())

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    cur.execute(
        f"insert into machines values('{mach['id']}','{mach['type']}','{mach['mem']}','{mach['image']}','stopped')")
    conBD.commit()
    conBD.close()

    src = os.path.abspath(os.path.join(DIR_IMAGES, mach["image"]))
    dst = os.path.abspath(os.path.join(DIR_MACHINES, mach["id"]))
    shutil.copyfile(src, dst)

    return mach


def startMachine(machId):
    print("startMach")
    _initDB()

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    machs = list(cur.execute(f"select * from machines where id = '{machId}'"))
    conBD.close()

    if len(machs) == 0: raise Exception("machine not found")
    mach = {
        "id": machs[0][0],
        "type": machs[0][1],
        "mem": machs[0][2],
        "image": machs[0][3],
        "state": machs[0][4]
    }

    if mach["state"] == "running": raise Exception("Machine running")

    # Check if there are connections associated
    connections = False
    try:
        connections = eval(listConnectionsByMachine(machId))
    except:
        print("No connections found")

    # Prepare the template for the machine
    f = open(os.path.join(DIR_TEMPLATES, "machine.xml"), "rt")
    template = f.read()
    dst = os.path.abspath(os.path.join(DIR_MACHINES, mach["id"]))
    template = template.replace("{{ID}}", "'" + mach["id"] + "'").replace("{{MEM}}", str(mach["mem"])).replace("{{IMG}}", dst).replace("{{NAME}}", mach["id"])

    # If the machine has connections associated with it, we replace the <interface> section in the original template
    if  connections:
        for con in connections:
            con_template = f"""
                            <interface type='network'>
                                <mac address='{con["Mac"]}'/>
                                <source network='{con["Network"]}'/>
                                <model type='virtio'/>
                            </interface>
                            """
            con_template += con_template
            
        template = template.replace("{{INTER}}", con_template)

    con = libvirt.open("qemu:///system")
    dom = con.createXML(template)
    con.close()
    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()

    cur.execute(f"update machines set state='running' where id='{mach['id']}'")

    mach["state"] = "running"
    conBD.commit()
    conBD.close()


def stopMachine(machId):
    print("stopMach")
    _initDB()

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    machs = list(cur.execute(f"select * from machines where id = '{machId}'"))
    conBD.close()

    # Check if exists
    if len(machs) == 0: raise Exception("machine not found")
    mach = {
        "id": machs[0][0],
        "type": machs[0][1],
        "mem": machs[0][2],
        "image": machs[0][3],
        "state": machs[0][4]
    }

    # Check its state
    if mach["state"] == "stopped": raise Exception("Machine already stopped")

    # Shutdown
    con = libvirt.open("qemu:///system")
    dom = con.lookupByName(machId)
    dom.destroy()
    con.close()

    # Update BBDD
    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    cur.execute(f"update machines set state='stopped' where id='{mach['id']}'")
    mach["state"] = "stopped"
    conBD.commit()
    conBD.close()


def removeMachine(machId):
    print("removeMach")
    _initDB()

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    machs = list(cur.execute(f"select * from machines where id = '{machId}'"))
    conBD.close()

    # Check if exists
    if len(machs) == 0: raise Exception("machine not found")
    mach = {
        "id": machs[0][0],
        "type": machs[0][1],
        "mem": machs[0][2],
        "image": machs[0][3],
        "state": machs[0][4]
    }

    # In case the machine is running we stop it
    if mach["state"] == "running":
        stopMachine(mach["id"])

    # We remove the machine file
    dst = os.path.abspath(os.path.join(DIR_MACHINES, mach["id"]))
    os.remove(dst)

    # Remove from BBDD
    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    cur.execute(f"DELETE FROM machines WHERE id='{mach['id']}'")
    mach["state"] = "removed"
    conBD.commit()
    conBD.close()


def listMachines(query):
    print("listMach")
    _initDB()

    ret = []
    colum_names = ["id", "type", "mem", "image", "state"]
    sql_query = ""

    # Prepare the query that we just recived
    for key, value in query.items():
        sql_query += f"{key} = {value} AND"

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()

    if len(sql_query) == 0:
        machs = list(cur.execute(f"select * from machines"))
    else:
        # We need to remove the last AND
        sql_query = sql_query[:-3]
        machs = list(cur.execute(f"select * from machines where {sql_query}"))

    conBD.close()

    for row in machs:
        ret.append(dict(zip(colum_names, row)))

    return json.dumps(ret)


##########################################################
####################### Conections #######################
##########################################################


def addConnection(machId, netId):
    print("addConn")
    _initDB()

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    machs = list(cur.execute(f"select * from machines where id = '{machId}'"))
    nets = list(cur.execute(f"select * from networks where id = '{netId}'"))
    conBD.close()

    # Check if machines exists
    if len(machs) == 0: raise Exception("machine not found")
    mach = {
        "id": machs[0][0],
        "type": machs[0][1],
        "mem": machs[0][2],
        "image": machs[0][3],
        "state": machs[0][4]
    }

    # Stop machine in case is running
    if mach["state"] == "running":
        stopMachine(mach["id"])

    # Check if there is network assosiacted
    if len(nets) == 0: raise Exception("There is no such network")
    connection = {
        "mach": machId,
        "net": netId,
        "mac": f"00:11:22:33:44:{randint(0, 9)}{randint(0, 9)}"  # Unless it is the exact format it won't work
    }

    # Add to BBDD
    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    cur.execute(f"insert into connections values('{connection['mac']}','{connection['mach']}','{connection['net']}')")
    conBD.commit()
    conBD.close()

    return connection


def removeConnection(machId, netId):
    print("removeConn")
    _initDB()

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    machs = list(cur.execute(f"select * from machines where id = '{machId}'"))
    nets = list(cur.execute(f"select * from networks where id = '{netId}'"))
    conBD.close()

    # Check if machines exists
    if len(machs) == 0: raise Exception("machine not found")
    mach = {
        "id": machs[0][0],
        "type": machs[0][1],
        "mem": machs[0][2],
        "image": machs[0][3],
        "state": machs[0][4]
    }

    # Stop machine in case is running
    if mach["state"] == "running":
        stopMachine(mach["id"])

    # Check if there is network assosiacted
    if len(nets) == 0: raise Exception("There is no such network")

    # Delete to BBDD
    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    cur.execute(f"DELETE FROM connections WHERE machine='{machId}' AND network='{netId}'")
    conBD.commit()
    conBD.close()


def listConnectionsByMachine(machId):
    print("listConMach")
    _initDB()

    colum_names = ["Mac", "Machine", "Network"]
    ret = []

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    connections = list(cur.execute(f"select * from connections where machine = '{machId}'"))
    conBD.close()

    # Check if connections exists
    if len(connections) == 0: raise Exception("There are no connections associated with that machine")

    for row in connections:
        ret.append(dict(zip(colum_names, row)))

    return json.dumps(ret)


def listConnectionsByNetwork(netId):
    print("listConNet")
    _initDB()

    colum_names = ["Mac", "Machine", "Network"]
    ret = []

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    connections = list(cur.execute(f"select * from connections where network = '{netId}'"))
    conBD.close()

    # Check if connections exists
    if len(connections) == 0: raise Exception("There are no connections associated with that machine")

    for row in connections:
        ret.append(dict(zip(colum_names, row)))

    return json.dumps(ret)


########################################################
####################### Networks #######################
########################################################

def addNetwork(net):
    print("addNet")
    _initDB()

    print(1)

    if "type" not in net: raise Exception("Missing type")
    if "addr" not in net: raise Exception("Missing addr")
    if "range" not in net: raise Exception("Missing range")
    if "bridge" not in net: raise Exception("Missing bridge")

    print(2)

    net["id"] = str(time.time_ns())

    # Create the network on libvirt. On thing to point out is the split in the range od the ip, the format desired is as
    # follows:
    # 192.168.1.1-192.168.1.255
    template = f"""
            <network>
              <name>{net["id"]}</name>
              <bridge name="{net["bridge"]}" />
              <forward mode="{net["type"]}"/>
                 <ip address="{net["addr"]}" netmask="255.255.255.0">
                    <dhcp>
                      <range start="{net["range"].split("-")[0]}" end="{net["range"].split("-")[1]}" />
                    </dhcp>
                 </ip>
            </network>"""

    con = libvirt.open("qemu:///system")
    dom = con.networkCreateXML(template)
    con.close()

    print(3)

    # Insert in DB
    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    cur.execute(
        f"insert into networks values('{net['id']}','{net['type']}','{net['addr']}','{net['range']}','{net['bridge']}')")
    conBD.commit()
    conBD.close()

    return net


def removeNetwork(netId):
    print("removeNet")
    _initDB()

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    nets = list(cur.execute(f"select * from networks where id = '{netId}'"))
    connnections = list(cur.execute(f"select * from connections where network = '{netId}'"))
    conBD.close()

    # Check if the network exists
    if len(nets) == 0: raise Exception("There is no such network")
    # Check if there are connections associated
    if len(connnections) > 0: raise Exception("There are connections associated with this network, remove them first")

    con = libvirt.open("qemu:///system")
    dom = con.networkLookupByName(netId)
    dom.destroy()
    con.close()

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()
    cur.execute(f"DELETE FROM networks WHERE id = '{netId}'")
    conBD.commit()
    conBD.close()


def listNetworks(query):
    print("listNet")
    _initDB()

    ret = []
    colum_names = ["id", "type", "addr", "range", "bridge"]
    sql_query = ""

    # Prepare the query that we just recived
    for key, value in query.items():
        sql_query += f"{key} = {value} AND"

    conBD = sqlite3.connect(FILE_DB)
    cur = conBD.cursor()

    if len(sql_query) == 0:
        nets = list(cur.execute(f"select * from networks"))
    else:
        # We need to remove the last AND
        sql_query = sql_query[:-3]
        nets = list(cur.execute(f"select * from networks where {sql_query}"))

    conBD.close()

    for row in nets:
        ret.append(dict(zip(colum_names, row)))

    return json.dumps(ret)
