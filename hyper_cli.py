import sys
import requests
import json

HELP_MSG = '''
            Usage: python hyper_cli.py <command> [options]
                Commands:
                add <image> <type> <mem>
                connect <machid> <netid>
                disconnect <machid> <netid>
                start <id>
                stop <id>
                rm <id>
                list {field=value}
                net add <type> <addr> <range> (Example: 192.168.1.1-192.168.1.255) <bridge>
                net rm <id>
                net list <{field=value}>
            '''

def cli_help():
    print(HELP_MSG)


###################################
##           Funciones           ##
###################################

##         -- Machines --        ##

def addMachine(image, type, mem):
    print(f'Add machine {image} {type} {mem}')
    machine= {"image": image, "type": type, "mem":mem}
    resp=requests.post(url='http://localhost:5000/hyper/machines', json=machine)
    print(resp)


def startMachine(machId):
    print(f'Start machine {machId}')
    status= {"state": "running"}
    resp=requests.put(url=f'http://localhost:5000/hyper/machines/{machId}', json=status)
    print(resp)


def stopMachine(machId):
    print(f'Stop machine {machId}')
    status= {"state": "stopped"}
    resp=requests.put(url=f'http://localhost:5000/hyper/machines/{machId}', json=status)
    print(resp)


def removeMachine(machId):
    print(f'Remove Machine {machId}')
    resp=requests.delete(url=f'http://localhost:5000/hyper/machines/{machId}')
    print(resp)


def listMachine(query):
    print(f'List {query}')
    query=json.loads(query)
    resp=requests.get(url=f'http://localhost:5000/hyper/machines', json=query)
    print(resp)
    machList=resp.json()
    for mach in machList:
        print(mach)


    ##         -- Network --         ##

def addNetwork(type, addr, range, bridge):
    print(f"Add Network {type} {addr} {range} {bridge}")
    net={"type":type, "addr":addr, "range":range, "bridge":bridge}
    resp=requests.post(url=f'http://localhost:5000/hyper/networks', json=net)
    print(resp)


def listNetwork(query):
    print(f'List Network {query}')
    query=json.loads(query)
    resp=requests.get(url=f'http://localhost:5000/hyper/networks', json=query)
    netList=resp.json()
    for net in netList:
        print(net)


def removeNetwork(netId):
    print(f'Remove Network {netId}')
    resp=requests.delete(url=f'http://localhost:5000/hyper/networks/{netId}')
    print(resp)


    ##         -- Connections --         ##

def addConnection(machId,netId):
    print(f'Add Connection {machId} {netId}')
    net={"network":netId}
    resp=requests.post(url=f'http://localhost:5000/hyper/machines/{machId}/connections', json=net)
    print(resp)


def removeConnection(machId,netId):
    print(f'Remove Connection {machId} {netId}')
    resp=requests.delete(url=f'http://localhost:5000/hyper/machines/{machId}/connections/{netId}')
    print(resp)


    ###################################
    ##         Logica script         ##
    ###################################

if sys.argv[1] == "add":
    print(1)
    print(len(sys.argv))
    print(2)
    if len(sys.argv) != 5:
        cli_help()
        len(sys.argv)
    else:
        addMachine(sys.argv[2],sys.argv[3],sys.argv[4])

elif sys.argv[1] == "start":
    if len(sys.argv) != 3:
        cli_help()
    else:
        startMachine(sys.argv[2])

elif sys.argv[1] == "stop":
    if len(sys.argv) != 3:
        cli_help()
    else:
        stopMachine(sys.argv[2])

elif sys.argv[1] == "rm":
    if len(sys.argv) != 3:
        cli_help()
    else:
        removeMachine(sys.argv[2])

elif sys.argv[1] == "list":
    if len(sys.argv) != 3:
        cli_help()
    else :
        listMachine(sys.argv[2])

elif sys.argv[1] == "net":
    if sys.argv[2] == "add":
        if len(sys.argv) != 7:
            cli_help()
        else:
            addNetwork(sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6])

    if sys.argv[2] == "list":
        if len(sys.argv) != 4:
            cli_help()
        else:
            listNetwork(sys.argv[3])

    if sys.argv[2] == "rm":
        if len(sys.argv) != 4:
            cli_help()
        else:
            removeNetwork(sys.argv[3])

elif sys.argv[1] == "connect":
    if len(sys.argv) != 4:
        cli_help()
    else:
        addConnection(sys.argv[2],sys.argv[3])

elif sys.argv[1] == "disconnect":
    if len(sys.argv) != 4:
        cli_help()
    else:
        removeConnection(sys.argv[2],sys.argv[3])

else:
    cli_help()

