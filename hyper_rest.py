from flask import Flask, request, jsonify
import json
import hyper

app = Flask(__name__)


###################################
##           Machines            ##
###################################

@app.route("/hyper/machines", methods=["POST"])
def addMachine():
    print("Add Machine")
    mach = request.get_json()
    return hyper.addMachine(mach)


@app.route("/hyper/machines/<machId>", methods=["PUT"])
def updateMachine(machId):
    mach = request.get_json()
    print(f'update Machine {mach["state"]}')

    if mach["state"] == "running":
        hyper.startMachine(machId)
    elif mach["state"] == "stopped":
        hyper.stopMachine(machId)

    return mach


@app.route("/hyper/machines/<machId>", methods=["DELETE"])
def removeMachine(machId):
    print("remove Machine")
    hyper.removeMachine(machId)
    return {"info": "Success"}, 200


@app.route("/hyper/machines")
def listMachines():
    print("List Machines")
    query = request.get_json()
    return hyper.listMachines(query)


@app.route("/hyper/machines/<machId>")
def getMachine(machId):
    print("get Machine")
    return hyper.listMachines({"id": machId})


###################################
##           Networks            ##
###################################

@app.route("/hyper/networks", methods=["GET"])
def listNetworks():
    print("list Networks")
    query = request.get_json()
    return hyper.listNetworks(query)


@app.route("/hyper/networks/<netId>", methods=["GET"])
def getNetwork(netId):
    print("get Network")
    return hyper.listNetworks({"id": netId})


@app.route("/hyper/networks", methods=["POST"])
def addNetwork():
    print("add Network")
    net = request.get_json()
    return hyper.addNetwork(net)


@app.route("/hyper/networks/<netId>", methods=["DELETE"])
def removeNetwork(netId):
    print("remove Network")
    hyper.removeNetwork(netId)
    return {"info": "Success"}, 200


###################################
##           Connections         ##
###################################

@app.route("/hyper/machines/<machId>/connections", methods=["GET"])
def getConnectionsByMach(machId):
    print(f"get Connections {machId}")
    return hyper.listConnectionsByMachine(machId)


@app.route("/hyper/machines/<machId>/connections/<netId>", methods=["GET"])
def getConnectionByMach(machId, netId):
    print(f"get Connections {machId} and {netId}")
    cons = hyper.listConnectionsByMachine(machId)

    for con in cons:
        if con["Network"] == netId:
            return con

    return {"error": "not found"}, 400


@app.route("/hyper/machines/<machId>/connections", methods=["POST"])
def addConnectionByMach(machId):
    print(f"add Connection {machId}")
    net = request.get_json()
    return hyper.addConnection(machId,net["network"])


@app.route("/hyper/machines/<machId>/connections/<netId>", methods=["DELETE"])
def removeConnectionByMach(machId, netId):
    print(f"remove Connection {machId} and {netId}")
    hyper.removeConnection(machId, netId)
    return {"info": "Success"}, 200


@app.route("/hyper/networks/<netId>/connections", methods=["GET"])
def getConnectionsByNet(netId):
    print(f"get Connections {netId}")
    return hyper.listConnectionsByNetwork(netId)


@app.route("/hyper/networks/<netId>/connections/<machId>", methods=["GET"])
def getConnectionByNet(machId, netId):
    print(f"get Connections {machId} and {netId}")
    cons = hyper.listConnectionsByNetwork(netId)

    for con in cons:
        if con["Machine"] == machId:
            return con

    return {"error": "not found"}, 400


@app.route("/hyper/networks/<netId>/connections", methods=["POST"])
def addConnectionByNet(netId):
    print(f"add Connection {netId}")
    mach = request.get_json()
    return hyper.addConnection(mach["machine"], netId)


@app.route("/hyper/networks/<netId>/connections/<machId>", methods=["DELETE"])
def removeConnectionByNet(machId, netId):
    print(f"remove Connection {machId} and {netId}")
    hyper.removeConnection(machId, netId)
    return {"info": "Success"}, 200